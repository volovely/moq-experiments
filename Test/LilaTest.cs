﻿using Moq;
using MoqTest.Implementation;
using MoqTest.Interfaces;
using NUnit.Framework;
using System;

namespace Test
{
    [TestFixture]
    public class LilaTest
    {
        [Test]
        public void LoveFryTest()
        {
            var lila = new Lila();
            var fryMock = new Mock<IFry>(MockBehavior.Strict);
            fryMock.Setup(f => f.LoveLila(It.IsAny<string>()));
            lila.Fry = fryMock.Object;

            lila.LoveFry();

            fryMock.Verify(f => f.LoveLila(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void LoveZepTest()
        {
            var lila = new Lila();
            var fryMock = new Mock<IFry>(MockBehavior.Strict);
            fryMock.Setup(f => f.ToBeStupid());
            lila.Fry = fryMock.Object;

            lila.LoveZep();

            fryMock.Verify(f => f.ToBeStupid(), Times.Once);
        }
    }
}
