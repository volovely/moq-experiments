﻿using MoqTest.Interfaces;
using System;

namespace MoqTest.Implementation
{
    public class Fry : IFry
    {
        public void ToBeStupid()
        {
            Console.WriteLine("stupid Fry");
        }

        public void LoveLila(string str)
        {
            Console.WriteLine(str);
        }
    }
}
