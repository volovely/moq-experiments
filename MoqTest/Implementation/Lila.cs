﻿using MoqTest.Interfaces;

namespace MoqTest.Implementation
{
    public class Lila : ILila
    {
        public IFry Fry { private get; set; }

        public void LoveFry()
        {
            Fry?.LoveLila("Lila love Fry");
        }

        public void LoveZep()
        {
            Fry?.ToBeStupid();
        }
    }
}
