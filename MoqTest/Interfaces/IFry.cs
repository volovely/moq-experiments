﻿

namespace MoqTest.Interfaces
{
    public interface IFry
    {
        void ToBeStupid();
        void LoveLila(string str);
    }
}
